cat  <<XXX
    __  __                 ____              ____  _____
   / / / /___  ____  ___  / __ \__  ______  / __ \/ ___/
  / /_/ / __ \/ __ \/ _ \/ /_/ / / / / __ \/ / / /\__ \\
 / __  / /_/ / /_/ /  __/ _, _/ /_/ / / / / /_/ /___/ /
/_/ /_/\____/ .___/\___/_/ |_|\__,_/_/ /_/\____//____/
           /_/
XXX

#内网穿透方案


1. sshd_config GatewayPorts yes #保证端口转发到 0 地址 
2. ssh-copy #ssh 无密码登录
3. autossh 设置自动重连
```
# ssh 保证心跳 
export AUTOSSH_LOGLEVEL=7 && export AUTOSSH_DEBUG=1 && \
autossh -M 0 -o ServerAliveInterval=3 -o ServerAliveCountMax=1 -N -R 172.17.0.1:2222:172.17.0.2:22  song@172.17.0.1 

# M 保证心跳 # prefer this 
export AUTOSSH_LOGLEVEL=7 && export AUTOSSH_DEBUG=1 && \
autossh -M 20000 -N -R 172.17.0.1:2222:172.17.0.2:22  song@172.17.0.1 

```
