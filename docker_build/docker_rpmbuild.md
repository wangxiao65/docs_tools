# rpmbuild in docker

## in vm
1. docker pull sugarfillet/openeuler:aarch64 
2. docker run -itdi -v /mnt:/mnt sugarfillet/openeuler:aarch64 bash 
3. docker exec -it ${container_id} bash 
## in docker 
4. config openeuler mirrors [1]
5. dnf install -y less vim rpm-build 'dnf-command(download)' 'dnf-command(builddep)'
6. yumdownloader --source PKG # get PKG.src.rpm
7. rpm-build PKG


[1]:(here) 
```
cat > /etc/yum.repos.d/a.repo <<eof
[mainline]
name=mainline
baseurl=http://119.3.219.20:82/openEuler:/Mainline/standard_aarch64/
enabled=1
gpgcheck=0
priority=1

[bringIn]
name=bringIn
baseurl=http://119.3.219.20:82/bringIn/standard_aarch64/
enabled=1
gpgcheck=0
priority=2

[bringInRely]
name=bringInRely
baseurl=http://119.3.219.20:82/bringInRely/standard_aarch64/
enabled=1
gpgcheck=0
priority=3
eof

```
 

