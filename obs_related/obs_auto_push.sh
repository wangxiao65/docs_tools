#!/bin/bash

if [[ 2 != $# ]];then
	echo $0 PKG_LIST PROJ_TO
	exit 2
fi

SRC='src/'
file=$1
proj=$2
repo_n='shuai.repo'

if [[ ! -d $SRC ]];then
	mkdir $SRC
fi

#init proj
osc co $proj

# config fedora & fedora-update source 
pushd /etc/yum.repos.d/

cat > $repo_n << EOF 
[fedora-u-src]
name=fedora-u-src
baseurl=http://mirrors.aliyun.com/fedora/updates/29/Everything/SRPMS
enabled=1
gpgcheck=0
priority=5
[fedora-src]
name=fedora-src
baseurl=http://mirrors.aliyun.com/fedora/releases/29/Everything/source/tree
enabled=1
gpgcheck=0
priority=4
EOF


popd

for x in `cat $file`;do
# download src.rpm from fc
yumdownloader --source --repo fedora-src --repo fedora-u-src $x && mv $x-*.src.rpm $SRC \
&& osc importsrcpkg -p $proj -c -n $x $SRC/$x-*.src.rpm 
done

rm -f /etc/yum.repos.d/$repo_n
