#!/bin/bash
set -x

#pkg=$(basename $(pwd))
#[[ ! -d test ]] && mkdir -p test/{old,new}
#cp /mnt/b/home/abuild/rpmbuild/RPMS/*/*.rpm ./test/new
#osc getbinaries openEuler:function $pkg standard_aarch64 aarch64 -d ./test/old

pushd test
for x in new old ;do pushd $x &>/dev/null; rpm -qp -l *.rpm | sort > ./${x}_list; rpm -qp --provides *.rpm | sort > ./${x}_prv; popd &>/dev/null;done
vimdiff old/old_list new/new_list
vimdiff old/old_prv new/new_prv
popd

# install/update/erase
pushd test/old
dnf install -y *.rpm 
popd

pushd test/new
a=($(ls *.rpm)) 
rpm -Uvh *.rpm 
popd

set -a b
count=0
for x in "${a[@]}";do
	name=$(echo $x | sed 's/-[[:digit:]].*//')
	b[$count]="$name"
	((count++))
done
rpm -evh "${b[@]}"
