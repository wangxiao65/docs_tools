if [[ 1 != $# ]];then
	echo "$0 LIST"
	exit 2
fi
printf  "%-90s%-10s\n" "PKG" "LINES"
for x  in `cat $1`;do 
	num=0
	repo=$(osc search $x |grep function| head -n1 | awk '{print $1}')
	[[ -z $repo ]] &&  printf  "%-90s%-10s\n" $x $num && continue  
	[[ ! -d $repo/$x ]] && osc co $repo $x -S &>/dev/null   
	#count
	pushd $repo/$x &>/dev/null
	num=$(cat *.spec | grep -v -e '^$' -e '^#'| sed '/^%changelog/,$d'| wc -l)
	popd &>/dev/null

	printf  "%-90s%-10s\n" $x $num 	
done > .abc
cat .abc | sort -nk2
