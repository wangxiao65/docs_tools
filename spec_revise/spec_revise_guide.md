# openeuler spec整改指北
- [openeuler spec整改指北](#openeuler-spec%e6%95%b4%e6%94%b9%e6%8c%87%e5%8c%97)
	- [常用网址](#%e5%b8%b8%e7%94%a8%e7%bd%91%e5%9d%80)
	- [OBS配置](#obs%e9%85%8d%e7%bd%ae)
	- [SPEC整改](#spec%e6%95%b4%e6%94%b9)
	- [敏感信息排查](#%e6%95%8f%e6%84%9f%e4%bf%a1%e6%81%af%e6%8e%92%e6%9f%a5)
	- [容器验证](#%e5%ae%b9%e5%99%a8%e9%aa%8c%e8%af%81)

## 常用网址

- rpm的运行机制：		https://fedoraproject.org/wiki/How_to_create_an_RPM_package/zh-cn
- 码云地址：			https://gitee.com/organizations/src-openeuler/projects
- obs地址：			https://117.78.1.88/project
- opensuse官网地址：	https://build.opensuse.org/
- fedora官网地址：	https://src.fedoraproject.org/
- 取包地址：
  - https://mirrors.aliyun.com/fedora/releases/29/Everything/source/tree/Packages/g/：  上面网址有问题用这个
  - https://dl.fedoraproject.org/pub/fedora/linux/releases/29/Everything/source/tree/Packages/g/
  - https://dl.fedoraproject.org/pub/fedora/linux/updates/29/Everything/SRPMS/Packages/g/
## OBS配置
- obs 工程说明 https://117.78.1.88/project/

	1. 当前我们只使用三个工程，mainline，bringIn，bringInRely
	2. bringln工程中的包为运行包和运行包的安装依赖。他依赖的工程有自己，mainline和bringInRely.
	3. bringInRely工程中的包为bringIn的编译依赖。他依赖的工程有自己，mainline，bingIn和其他的第三方包
	4. 表格中一阶段的包对应bringIn工程，二三阶段的包对应bringInRely工程，剩余的暂不处理就是extra里面剩下来的。extra工程我们暂时不用了。
- osc configure 
```
cat /root/.oscrc
[general]
apiurl = https://obs.openeuler.org
no_verify = 1
build-root = /root/osc/buildroot

[https://obs.openeuler.org]
user=xxxx
pass=yyyy
xxxx为obs注册账号，pass为密码
cat /etc/hosts
117.78.1.88 obs.openeuler.org
```
## SPEC整改
- 蓝云配置Yum源
```
cat /etc/yum.repos.d/openEuler_aarch64.repo
[mirrors]
name=mirrors
baseurl= http://mirrors.aliyun.com/fedora/releases/29/Everything/aarch64/os/
enabled=1
gpgcheck=0
priority=4

[mainline]
name=mainline
baseurl=http://119.3.219.20:82/openEuler:/Mainline/standard_aarch64/
enabled=1
gpgcheck=0
priority=1

[bringIn]
name=bringIn
baseurl=http://119.3.219.20:82/bringIn/standard_aarch64/
enabled=1
gpgcheck=0
priority=2

[bringInRely]
name=bringInRely
baseurl=http://119.3.219.20:82/bringInRely/standard_aarch64/
enabled=1
gpgcheck=0
priority=3

# yum clean all && yum makecache
```

- rpmbuild                      
>右键复制链接，在机子建一个目录并wget,得到源码包**.src.rpm
				解压源码包：rpm -ivh *.src.rpm  ===>  解压到/root/rpmbuild。spec文件在SPECS文件夹，其他文件在SOURCES，这部分得去看rpm运行机制
				#整改前rpmbuild -ba 原始.spec,得到二进制，复制到某个目录
		  
> 进入/root/rpmbuild/SPECS,整改spec,先备份一个（cp xxx.spec xxx.spec.old）:
			    %prep之前：     对齐，release号、description和summary自己写(可引用官网、rpmfind、opensuse)、注释删掉等
				%prep-%install: 重点改的地方，目的降低与fedora的重复度。自己写spec专业性太高，我们只能从格式入手。
				%files-%changelog：合行、变形、格式化等
参考spec编写指导


> 编译：
				      rpmbuild -ba **.spec，编译出二进制包，可进入/root/rpmbuild/RPMS/aarch64找到二进制包(有时是noarch)。
				      #若缺依赖： dnf install -y less vim rpm-build 'dnf-command(download)' 以及 dnf builddep **.spec
				
>验证：
					4个验证规则：1编译通过、2文件个数相等、3文件提供的功能（provides）相等、4功能验证
						2,3的验证可以见附1：
							与原始二进制文件个数相等：rpm -qpl *.rpm > a.txt
							与原始二进制文件provides相等：rpm -pq --provides *.rpm >> a.txt
							vimdiff  a.txt b.txt
						安装验证，升级验证，rpm -ivh 安装修改之前的包，rpm -Uvh 升级修改之后的二进制包。如有二进制命令 /usr/bin/xx -v 或者-h看下是否正常
			
- osc编译
	1. osc co bringIn(仓库名) freeipmi(包名)			   进入包名所在目录
	2. osc up -S
	3. 删掉网页的_services ,
    1. 执行以下shell脚本，去掉文件前缀。
		```bash
		rm -f _service;for file in `ls | grep -v .osc`;do new_file=${file##*:};mv $file $new_file;done
		```

    5. osc up 	
	6. spec整改
	7. osc add 
	8. 编译
	
		本地编译   osc build --no-verify --root=/tmp/yzh   
		编译日志最后有二进制所在目录且/tmp/yzh里面也有类似/root/rpmbuild的目录

     - 若obs上无代码：
		1. 先branch一个个人分支
		2. osc co 个人仓库名 个人包名
		3. 进入个人包名，源码包解压后的文件放进来。
		4. osc ci -m "any info"

## 敏感信息排查

敏感信息排查策略对齐：
    
	1. tar包必须要找到来源:进spec，查看source是否有效，无效则去github找
    1. 对于补丁文件，只能合入bugfix和必须的功能，可参考suse，suse有的话一般可以留
       【千万不能合入rhel，fedora，centos等厂商的定制补丁】
    2. 其他
    [.conf]如果是rhel，fedora自己加的定制的配置文件，建议使用社区的配置文件。
    [.service]对于systemd管理的service的话需要保留，如果就是正常的系统配置，不和rhel，fedora强相关，可以保留或略做修改。
    [.sh]sh多半是rhel和fedora的定制，看一下是不是必须（也可以参考suse），可以删除的话就删掉。



## 容器验证

1. docker pull sugarfillet/openeuler:aarch64           改镜像名：docker images、 docker tag image_id name
2. docker run -itd --name  openeuler1 sugarfillet/openeuler:aarch64 bash  可dockers ps找到container_id
3. docker exec -it ${container_id}或者 openeuler1(容器名字) bash
4. 进入容器，执行四部验证
