# spec整改明细
  - [开头定义的宏](#%e5%bc%80%e5%a4%b4%e5%ae%9a%e4%b9%89%e7%9a%84%e5%ae%8f)
  - [spec中的缩进统一格式，使用空格，保持对齐](#spec%e4%b8%ad%e7%9a%84%e7%bc%a9%e8%bf%9b%e7%bb%9f%e4%b8%80%e6%a0%bc%e5%bc%8f%e4%bd%bf%e7%94%a8%e7%a9%ba%e6%a0%bc%e4%bf%9d%e6%8c%81%e5%af%b9%e9%bd%90)
  - [软件包依赖](#%e8%bd%af%e4%bb%b6%e5%8c%85%e4%be%9d%e8%b5%96)
  - [预处理阶段（%prep）](#%e9%a2%84%e5%a4%84%e7%90%86%e9%98%b6%e6%ae%b5prep)
  - [编译阶段（%build）](#%e7%bc%96%e8%af%91%e9%98%b6%e6%ae%b5build)
  - [安装阶段（%install）](#%e5%ae%89%e8%a3%85%e9%98%b6%e6%ae%b5install)
  - [%file阶段](#file%e9%98%b6%e6%ae%b5)
  - [拆包](#%e6%8b%86%e5%8c%85)
## 开头定义的宏
```
宏(Macros)是使用变量和功能来控制rpm和rpmbuild的自定义的行为，宏指令一般地定义在 spec文件中。

语法:%define macro value。
rpm定义了很多宏，默认设置为/usr/lib/rpm/macros or /etc/rpm/macros.dist
可以使用rpm --showrc 查看当前的宏定义
也可以使用rpm --eval %{?_smp_mflags}查看宏定义的值


查看openEuler自定义的宏：https://10.175.102.91/project/prjconf/openEuler:BaseOS


%define testMacro 2
定义了一个宏，名称为testMecro，值为2，要使用这个宏，使用%{testMacro}或者%testMacro
global用来定义变量，方便开关。


%if 0%{?rhel}（是否定义）
%endif

如果%{rhel}被定义了，则 %{?rhel} 返回 %{rhel}，不然%{?rhel}视为未定义。
所以，当%{rhel}被定义过的话， 0%{?rhel}就执行这个if block；
反之，0%{?rhel}等于0，不执行这个if block。
```

## spec中的缩进统一格式，使用空格，保持对齐

```
Name：        #软件包名字，保持
Version：     #软件版本号，保持
Release：     #软件包发行号，变更。例如：RElease:4%{?dist}->5
Summary：     #概要，变更。一句话概括该软件包信息，打开URL查看软件包主页信息
License：     #变更：软件授权方式，多个License之间用and隔开。错误格式：LGPLv2 BSD，正确格式：LGPLv2 and BSD
Group：       #组信息，删除
URL：         #一般保持，需尝试登陆能否登上，无效的要替换成有效的
Source：      #保持，源码包的名字/下载地址，按顺序表示
Patch：       #补丁，需标明补丁来源，以保证补丁可溯性，保持
Description： 软件包具体描述--->description行尾要对齐

> 上面这些基础字段全部要对齐，只能使用空格不能用TAB，并且各关键字项前后顺序要与template模板完全一致，但是模板中有而实际spec文件中没有的字段（%check/%pre等）也不用多余地写出。spec中version、release尽量简化为单个数字，且保持递增，h和%{?dist}的后缀要去除。查看宏定义，意思是%{?dist}默认为空。openEuler会自动匹配当前芯片架构，所以%{?_isa}可以删除


说明：
0.宏要尽量展开。
1.基础字段全部使用空格保证对齐，且各项关键字顺序需与上述一致,开头的if判断尽量消除，测试和python3都打开
2.openEuler会自动匹配当前芯片架构，%{?_isa}可去除，比如%{name}%{?_isa}可以替换成%{name}
3.spec中version、release比较复杂的场景，如果没有特殊要求，尽量简化为单个数字，且保持递增，h和%{?dist}的后缀要去除，
4.调试的字段要去掉
5.注释要全部删掉。
```

## 软件包依赖
```
如果你的意图是只在某一个指定平台架构上构建，可以使用 exclusivearch 标签；
如果你还是想不在某个平台平台架构上构建，可以使用 excludearch 标签；

BuildRequires：定义build时所依赖的软件包，在构建编译软件包时需要的辅助工具。尽量写到一行，用空格隔开，gcc建议去掉，因为一般环境中已经存在，无需特意说明。且写包名即可，例如%{bindir}/man替换成man，---->要求100个字符左右。

Requires：定义安全时的依赖包，指二进制软件包在其他机器上的安装时，所需要依赖的其他软件包，尽量写成一行，用空格隔开。RreReq、Requires（pre）、

Requires（post）、Requires（preun）、Requires（postun）等都是针对不同阶段的依赖指定的，策略相同。  ---->要求100个字符左右

说明： 
1.该部分的依赖关系定义了一个软件包正常工作需要依赖其他软件包，在RPM包升级、安装和删除的时候需要确保依赖关系得到满足 
2.多个编译依赖或安装依赖可以汇总成1~3行，这样看起来简洁
```

## 预处理阶段（%prep）
```
该阶段描述了解压源码包的方法该,阶段首先读取位于%_sourcedir目录的源代码和patch。之后，解压源代码至%_builddir的子目录并应用所有patch。

可用选项：
① -n name :如果源码包解压后的目录名称与 RPM 名称不同，此选项用于指定正确的目录名称。例如，如果tarball 解压目录为FOO，则使用“%setup -n FOO”。
② -c name :如果源码包解压后包含多个目录，而不是单个目录时，此选项可以创建名为 name的目录，并在其中解压。

%patch命令：%patch0命令用于应用Patch0（%patch1应用Patch1，以此类推）。
① %patch -p1使用前面定义的Patch补丁进行，-p1是忽略patch的第一层目录。
② %Patch2 -p1 -b xxx.patch打上指定的补丁，-b是指生成备份文件

%autosetup命令：自动打补丁%autosetup -n %{name}-%{version} -p1
在该阶段常用命令：mv、find、chmod、cp

%setup -q# 解压源文件程序 patch # 应用对应补丁
> 推荐更改成%autosetup命令，自动解压源码包和打补丁
举例：
%prep
  setup -q -n Crypt-OpenSSL-Guess-%{version} 
修改后:
%prep 
  autosetup -n Crypt-OpenSSL-Guess-%{version} -p1 
```
## 编译阶段（%build）
```
编译位于%_builddir构建目录下的文件，这个阶段就是执行常见的configure和make操作。

这个阶段常见的指令：

%configure 相当于"./configure"，也可以指定参数,这个不是关键字，而是rpm定义的标准宏命令，意思是执行源代码的configure配置。这个宏还可以接受额外的参数，如果某些软件有某些高级特性需要开启，可以通过给%configure宏传参数来开启。

%make_build相当于make %{?_smp_mflags}，%{?_smp_mflags}的意思是：如果有多处理器，make时并行编译
make：#替换成%make_build

说明：configure、make等编译命令，选项如果没有宏控制，可以汇总到1行。
pushd表示进入某一目录，可以用cd替代。如果有多个阶段，要cd ..回到上一级目录。

%build 举例：

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
make %{?_smp_mflags}

修改后：
%build 
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build
```
## 安装阶段（%install）
```
%install阶段：把软件安装到虚拟的根目录中。这个阶段会在%buildrootdir目录里建好目录结构，然后将需要打包到 rpm 软件包里的文件从%builddir里拷贝到%_buildrootdir里对应的目录里。注意：%install部分使用的是绝对路径，而%file部分使用则是相对路径，虽然其描述的是同一个地方。%install主要就是为了后面的%file服务的。%{buildroot}和$RPM_BUILD_ROOT是等价的。

rm -rf $RPM_BUILD_ROOT（不需要，%install开始时会自动删除BUILDROOT内容）

%make_install 相当于make install DESTDIR=%{buildroot}

1.清空安装目录在安装时会自动清除，rm -rf %{buildroot}和rm -rf $RPM_BUILD_ROOT命令可以删除
2.删除*.la和.a文件命令：
rm %{buildroot}%{_libdir}/*.la
或者
find $RPM_BUILD_ROOT -type f -name "*.la" -delete \
find $RPM_BUILD_ROOT -type f -name "*.a" -delete \
可以用宏%delete_la and a调换
3.删除*.la文件：
find $RPM_BUILD_ROOT -type f -name "*.la" -delete
可以用宏%delete_la调换
这边修改成这样的话编译不过，待修改

举例：
%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*
修改后：
%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*
```
## %file阶段
```
%files阶段：本段是文件段，主要用来说明会将%{buildroot}目录下的哪些文件和目录最终打包到rpm包里。定义软件包所包含的文件，分为三类：
•	说明文档（doc）
•	配置文件（config）
•	执行程序
还可定义文件存取权限，拥有者及组别。

注意：这里会在虚拟根目录下进行，千万不要写绝对路径，而应用宏或变量表示相对路径。

%file是对软件打包时，打包的顺序要和前面定义package的顺序保持一致

在%files阶段的第一条命令的语法是：
%defattr(文件权限,用户名,组名,目录权限)

%files
%defattr (-,root,root,0755) ← 设定默认权限
%config(noreplace) /etc/my.cnf ← 表明是配置文件
%doc %{src_dir}/Docs/ChangeLog ← 表明这个是文档
%attr(644, root, root) %{_mandir}/man8/mysqld.8*  ← 分别是权限，属主，属组

在%files阶段的第一条命令的语法是：
%defattr(文件权限,用户名,组名,目录权限)
%files
%defattr (-,root,root,0755) ← 设定默认权限
%config(noreplace) /etc/my.cnf ← 表明是配置文件
%doc %{src_dir}/Docs/ChangeLog ← 表明这个是文档
%attr(644, root, root) %{_mandir}/man8/mysqld.8*  ← 分别是权限，属主，属组

%exclude列出不想打包到rpm中的文件。注意：如果%exclude指定的文件不存在，也会出错的。
在安装 rpm 时，会将可执行的二进制文件放在/usr/bin目录下，动态库放在/usr/lib或者/usr/lib64目录下，配置文件放在/etc目录下，并且多次安装时新的配置文件不会覆盖以前已经存在的同名配置文件。

关于%files阶段有两个特性：
①	%{buildroot}里的所有文件都要明确被指定是否要被打包到rpm里。什么意思呢？假如，%{buildroot}目录下有 4 个目录 a、b、c和d，在%files里仅指定 a 和 b 要打包到 rpm 里，如果不把 c 和 d 用exclude声明是要报错的；
②	 如果声明了%{buildroot}里不存在的文件或者目录也会报错。
关于%doc宏，所有跟在这个宏后面的文件都来自%{_builddir}目录，当用户安装 rpm 时，由这个宏所指定的文件都会安装到/usr/share/doc/name-version/目录
里。

示例：
①	同一目录下的多个文件，可以写到一行或可以用*匹配符替换

%package_help
%package help
Summary: Documents for %{name}
Buildarch: noarch
Requires: man info

%description help
Man pages and other related documents for %{name}.

%install_info() /sbin/install-info %1 %{_infodir}/dir || :

%install_info_rm() /sbin/install-info --remove %1 %{_infodir}/dir || :

举例：
%files 
%license LICENSE
%doc Changes README.md
%{perl_vendorlib}/*
%{_mandir}/man3/*
修改后：
%files 
%license LICENSE
%doc Changes README.md
%{perl_vendorlib}/*
%files help 
%{_mandir}/man3/*

这里说明下：
这里拆出来一个help包放Man的东西，README.md如果没有Linsence信息也放到help包。
拆包的时候在上面description要加一个%package_help这样的宏。
他其实代表：
%package help
Summary:       
Requires:
%description help

这几个关键字是成对出现的，不要漏掉。除了主包不需要%package +包名，子包都是需要的。
```
## 拆包

```
openEuelr一般拆成3个包主包，devel包,help包。
复杂的包继续拆分成多个。

拆包规则
1、主包包含命令，配置，本软件运行苏需要的so以及对外提供的动态库。license.copyright.author,readme(如果包含版权信息)，版权信息，法务全部房子lisence目录下
2、devel包包含：1、静态库.a 2、头文件3、Example 4、test用例 5、其他开发使用的内容
3、help包包含：1、二次开发文档，接口函数说明手册2、man 、info手册等相关文档

特殊场景需要考虑：
for-language包。本地化支持 其他复杂特殊包gcc Python2 Python3

软件包拆包并包原则
包名	包含内容
主包	①	lib包 ②命令运行所需的so ③License
Devel包	①	静态库.a ②头文件 ③Example ④tests用例 ⑤static包
Help包	①	man、info的相关文档②不含版权信息的README

说明：
1）运行相关（实现主要功能）的都收编合并为一个主包，包含命令、配置、so动态库、以及本软件对外提供的动态库
2）头文件/静态库.a（关键字%package static/%file static） 合并为devel包
3）帮助文档（man） 合并为help包
4）当收编一个包时，被收编包如有Requires依赖关系要继承（不删）,与被收编Package对应的Files描述全部删掉；添加Obsoletes声明丢弃原包、Proviedes来指明功能继承，实现兼容。
5）拆分出去的包（devel/help等），%files内容保留并保持一致；
```